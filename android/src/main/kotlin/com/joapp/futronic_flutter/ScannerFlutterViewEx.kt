package com.joapp.futronic_flutter

import android.content.Context
import android.widget.TextView
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView

class ScannerFlutterViewEx(val context: Context?, val messenger: BinaryMessenger, val id: Int) : PlatformView, MethodChannel.MethodCallHandler {
    private val textView = TextView(context)
    private val methodChannel = MethodChannel(messenger, "scanner_view_$id").apply {
        setMethodCallHandler(this@ScannerFlutterViewEx)
    }

    override fun getView() = textView
    override fun dispose() {}
    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            "setText" -> setText(call, result)
            else -> result.notImplemented()
        }
    }

    private fun setText(methodCall: MethodCall, result: MethodChannel.Result) {
        val text = methodCall.arguments as String
        textView.text = text
        result.success(null)
    }

}