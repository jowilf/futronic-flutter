package com.joapp.futronic_flutter

import android.content.Context
import com.futronictech.FingerView
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView

class FingerViewFlutter(val context: Context?, val messenger: BinaryMessenger, val id: Int) : PlatformView, MethodChannel.MethodCallHandler {
    private val fingerView = FingerView(context!!)
    private val methodChannel = MethodChannel(messenger, "scanner_view_$id").apply {
        setMethodCallHandler(this@FingerViewFlutter)
    }

    override fun getView() = fingerView
    override fun dispose() {}
    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            "setText" -> setText(call, result)
            "scan" -> {
                fingerView.scan()
                result.success(null)
            }
            "look" -> fingerView.look(result)
            "capture" -> result.success(fingerView.saveBitmap())
            else -> result.notImplemented()
        }
    }

    private fun setText(methodCall: MethodCall, result: MethodChannel.Result) {
        val text = methodCall.arguments as String
        fingerView.textView.text = text
        result.success(null)
    }

}