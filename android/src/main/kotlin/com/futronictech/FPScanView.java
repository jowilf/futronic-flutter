package com.futronictech;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

public class FPScanView {
    /* access modifiers changed from: private */
    public UsbDeviceDataExchangeImpl ctx = null;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    private ScanThread mScanThread;
    private FingerView fingerView;

    public FPScanView(UsbDeviceDataExchangeImpl context, Handler handler, FingerView fingerView) {
        this.mHandler = handler;
        this.ctx = context;
        this.fingerView = fingerView;
    }

    public synchronized void start() {
        if (this.mScanThread == null) {
            this.mScanThread = new ScanThread();
            this.mScanThread.start();
        }
    }

    public synchronized void stop() {
        if (this.mScanThread != null) {
            this.mScanThread.cancel();
            this.mScanThread = null;
        }
    }

    private class ScanThread extends Thread {
        private boolean bGetInfo;
        private boolean bRet;
        private Scanner devScan;
        private int errCode;
        private int flag;
        private int mask;
        private String strInfo;

        public ScanThread() {
            this.devScan = null;
            this.bGetInfo = false;
            this.devScan = new Scanner();
        }

        public void run() {
            boolean bRet2;
            while (!fingerView.mStop) {
                if (!this.bGetInfo) {
                    Log.i("FUTRONIC", "Run fp scan");
                    if (fingerView.mUsbHostMode) {
                        bRet2 = this.devScan.OpenDeviceOnInterfaceUsbHost(fingerView.usb_host_ctx);
                    } else {
                        bRet2 = this.devScan.OpenDevice();
                    }
                    if (!bRet2) {
                        if (fingerView.mUsbHostMode) {
                            FPScanView.this.ctx.CloseDevice();
                        }
                        FPScanView.this.mHandler.obtainMessage(1, -1, -1, this.devScan.GetErrorMessage()).sendToTarget();
                        FPScanView.this.mHandler.obtainMessage(4).sendToTarget();
                        return;
                    } else if (!this.devScan.GetImageSize()) {
                        FPScanView.this.mHandler.obtainMessage(1, -1, -1, this.devScan.GetErrorMessage()).sendToTarget();
                        if (fingerView.mUsbHostMode) {
                            this.devScan.CloseDeviceUsbHost();
                        } else {
                            this.devScan.CloseDevice();
                        }
                        FPScanView.this.mHandler.obtainMessage(4).sendToTarget();
                        return;
                    } else {
                        fingerView.InitFingerPictureParameters(this.devScan.GetImageWidth(), this.devScan.GetImaegHeight());
                        this.strInfo = this.devScan.GetVersionInfo();
                        FPScanView.this.mHandler.obtainMessage(2, -1, -1, this.strInfo).sendToTarget();
                        this.bGetInfo = true;
                    }
                }
                this.flag = 0;
                this.devScan.getClass();
                this.devScan.getClass();
                this.mask = 65;
                if (fingerView.mLFD) {
                    int i = this.flag;
                    this.devScan.getClass();
                    this.flag = i | 1;
                }
                if (fingerView.mInvertImage) {
                    int i2 = this.flag;
                    this.devScan.getClass();
                    this.flag = i2 | 64;
                }
                if (!this.devScan.SetOptions(this.mask, this.flag)) {
                    FPScanView.this.mHandler.obtainMessage(1, -1, -1, this.devScan.GetErrorMessage()).sendToTarget();
                }
                long lT1 = SystemClock.uptimeMillis();
                if (fingerView.mFrame) {
                    this.bRet = this.devScan.GetFrame(fingerView.mImageFP);
                } else {
                    this.bRet = this.devScan.GetImage2(4, fingerView.mImageFP);
                }
                if (!this.bRet) {
                    FPScanView.this.mHandler.obtainMessage(1, -1, -1, this.devScan.GetErrorMessage()).sendToTarget();
                    this.errCode = this.devScan.GetErrorCode();
                    int i3 = this.errCode;
                    this.devScan.getClass();
                    if (i3 != 4306) {
                        int i4 = this.errCode;
                        this.devScan.getClass();
                        if (i4 != 536870913) {
                            int i5 = this.errCode;
                            this.devScan.getClass();
                            if (i5 != 536870914) {
                                if (fingerView.mUsbHostMode) {
                                    this.devScan.CloseDeviceUsbHost();
                                } else {
                                    this.devScan.CloseDevice();
                                }
                                FPScanView.this.mHandler.obtainMessage(4).sendToTarget();
                                return;
                            }
                        }
                    }
                } else {
                    if (fingerView.mFrame) {
                        this.strInfo = String.format("NEW_FRAME. GetFrame time is %d(ms)", new Object[]{Long.valueOf(SystemClock.uptimeMillis() - lT1)});
                    } else {
                        this.strInfo = String.format("NEW_FRAME. GetImage2 time is %d(ms)", new Object[]{Long.valueOf(SystemClock.uptimeMillis() - lT1)});
                    }
                    FPScanView.this.mHandler.obtainMessage(1, -1, -1, this.strInfo).sendToTarget();
                }
                synchronized (fingerView.mSyncObj) {
                    FPScanView.this.mHandler.obtainMessage(3).sendToTarget();
                    try {
                        fingerView.mSyncObj.wait(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (fingerView.mUsbHostMode) {
                this.devScan.CloseDeviceUsbHost();
                return;
            } else {
                this.devScan.CloseDevice();
                return;
            }
        }

        public void cancel() {
            fingerView.mStop = true;
            try {
                synchronized (fingerView.mSyncObj) {
                    fingerView.mSyncObj.notifyAll();
                }
                join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
