package com.futronictech

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.joapp.futronic_flutter.R
import io.flutter.plugin.common.MethodChannel
import java.io.File
import java.io.FileOutputStream

class FingerView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {


    lateinit var imageView: ImageView
    lateinit var textView: TextView
    private var mBitmapFP: Bitmap? = null

    @JvmField
    var mImageFP: ByteArray? = null
    var lastFingerBytes: ByteArray? = null
    var mImageHeight = 0
    var mImageWidth = 0
    private var mCanvas: Canvas? = null
    private var mPaint: Paint? = null
    private var mPixels: IntArray? = null
    var mFPScan: FPScanView? = null
    var result: MethodChannel.Result? = null

    @JvmField
    var mStop = false

    @JvmField
    var mUsbHostMode = true

    @JvmField
    var usb_host_ctx: UsbDeviceDataExchangeImpl? = null

    @JvmField
    var mLFD: Boolean = false

    @JvmField
    var mInvertImage: Boolean = false


    @JvmField
    var mFrame: Boolean = true

    @JvmField
    var mSyncObj: Any = Any()

    init {
        LayoutInflater.from(context).inflate(R.layout.finger_view, this, true)
        imageView = this.findViewById(R.id.imageFinger)
        textView = this.findViewById(R.id.finger_msg)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    fun look(result: MethodChannel.Result) {
        this.result = result
    }

    fun scan() {
        mStop = true
        mFPScan?.stop()
        mStop = false
        usb_host_ctx?.CloseDevice()
        usb_host_ctx = UsbDeviceDataExchangeImpl(this.context, mHandler)
        if (usb_host_ctx?.OpenDevice(0, true) == true) {
            StartScan()
        } else if (usb_host_ctx?.IsPendingOpen() == false) {
            textView.text =
                    "Can not start scan operation.\nCan't open scanner device"
        } else {
            textView.text =
                    "..."
        }
    }

    fun InitFingerPictureParameters(wight: Int, height: Int) {
        mImageWidth = wight
        mImageHeight = height
        mImageFP = ByteArray(mImageWidth * mImageHeight)
        mPixels = IntArray(mImageWidth * mImageHeight)
        mBitmapFP = Bitmap.createBitmap(wight, height, Bitmap.Config.RGB_565);
        mCanvas = Canvas(mBitmapFP!!)
        mPaint = Paint()
        val cm = ColorMatrix()
        cm.setSaturation(0.0f)
        mPaint?.setColorFilter(ColorMatrixColorFilter(cm))
    }

    fun showBitMap() {
        for (i in 0 until mImageWidth * mImageHeight) {
            mPixels?.set(
                    i,
                    Color.rgb(mImageFP!![i].toInt(), mImageFP!![i].toInt(), mImageFP!![i].toInt())
            )
        }
        mCanvas?.drawBitmap(
                mPixels!!,
                0,
                mImageWidth,
                0,
                0,
                mImageWidth,
                mImageHeight,
                false,
                mPaint
        )
        imageView.setImageBitmap(mBitmapFP)
        imageView.invalidate()
        /* synchronized(mSyncObj) {
             mSyncObj.notifyAll()
         }*/
    }

    fun StartScan(): Boolean {
        mFPScan = FPScanView(usb_host_ctx, mHandler, this)
        mFPScan?.start()
        return true
    }

    fun saveBitmap(): String? {
        val imageName =
                "FINGER_" + System.currentTimeMillis().toString() + ".jpg"
        val outputDir = context.cacheDir
        val outputFile = File.createTempFile(imageName, "jpeg", outputDir)
        try {
            val out2 = FileOutputStream(outputFile)
            out2.write(
                    MyBitmapFile(
                            mImageWidth,
                            mImageHeight,
                            mImageFP
                    ).toBytes()
            )
            out2.close()
            textView.text = "Image is saved as $imageName"
            return outputFile.absolutePath
        } catch (e2: Exception) {
            textView.text = "Exception in saving file"
        }
        return null
    }

    private val mHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                1 -> {
                    Log.e("handler", "${msg.obj} (${msg.what})")
                    var code = msg.obj as String
                    if (code == "Empty Frame") {
                        lastFingerBytes?.let {
                            result?.success(MyBitmapFile(
                                    mImageWidth,
                                    mImageHeight,
                                    it
                            ).toBytes())
                            result = null
                            Log.e("handler", "send bytes ${Base64.encode(it, 0)}")
                        }
                        lastFingerBytes = null
                    } else if (code.startsWith("NEW_FRAME")) {
                        lastFingerBytes = mImageFP?.clone()
                    }
                    textView.text = msg.obj as String
                }
                2 -> {
                    textView.text = msg.obj as String
                }
                3 -> {
                    showBitMap()
                }
                4 -> {/*
                    FtrScanDemoUsbHostActivity.mButtonScan.isEnabled = true
                    this@FtrScanDemoUsbHostActivity.mCheckUsbHostMode.setEnabled(true)
                    FtrScanDemoUsbHostActivity.mButtonStop.isEnabled = false*/
                }
                255 -> if (usb_host_ctx?.ValidateContext() == true) {
                    textView.text = "Can't open scanner device"
                } else if (StartScan()) {
                    /* FtrScanDemoUsbHostActivity.mButtonScan.isEnabled = false
                     this@FtrScanDemoUsbHostActivity.mButtonSave.setEnabled(false)
                     this@FtrScanDemoUsbHostActivity.mCheckUsbHostMode.setEnabled(false)
                     FtrScanDemoUsbHostActivity.mButtonStop.isEnabled = true*/
                } else {
                    return
                }
                256 -> {
                    textView.text = "User deny scanner device"
                }
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        destroy()
    }

    fun destroy() {
        mStop = true
        if (mFPScan != null) {
            mFPScan?.stop()
            mFPScan = null
        }
        usb_host_ctx?.CloseDevice()
        usb_host_ctx?.Destroy()
        usb_host_ctx = null
    }
}