package com.futronictech;

public class MyBitmapFile {
    private byte[] m_BmpData;
    private BITMAPFILEHEADER m_fileHeaderBitmap = new BITMAPFILEHEADER();
    private BITMAPINFO m_infoBitmap = new BITMAPINFO();

    public MyBitmapFile() {
    }

    public MyBitmapFile(int nWidth, int nHeight, byte[] pImage) {
        int length = this.m_fileHeaderBitmap.sizeof() + this.m_infoBitmap.sizeof() + (nWidth * nHeight);
        this.m_fileHeaderBitmap.bfSize = length;
        this.m_fileHeaderBitmap.bfOffBits = this.m_fileHeaderBitmap.sizeof() + this.m_infoBitmap.sizeof();
        this.m_infoBitmap.bmiHeader.biWidth = nWidth;
        this.m_infoBitmap.bmiHeader.biHeight = nHeight;
        this.m_BmpData = new byte[length];
        byte[] TempData = this.m_fileHeaderBitmap.toBytes();
        System.arraycopy(TempData, 0, this.m_BmpData, 0, TempData.length);
        int offset = TempData.length;
        byte[] TempData2 = this.m_infoBitmap.bmiHeader.toBytes();
        System.arraycopy(TempData2, 0, this.m_BmpData, offset, TempData2.length);
        int offset2 = offset + TempData2.length;
        byte[] TempData3 = this.m_infoBitmap.bmiColors.GetGRBTableByteData();
        System.arraycopy(TempData3, 0, this.m_BmpData, offset2, TempData3.length);
        int offset3 = offset2 + TempData3.length;
        byte[] pRotateImage = new byte[(nWidth * nHeight)];
        int nImgOffset = 0;
        for (int iCyc = 0; iCyc < nHeight; iCyc++) {
            System.arraycopy(pImage, ((nHeight - iCyc) - 1) * nWidth, pRotateImage, nImgOffset, nWidth);
            nImgOffset += nWidth;
        }
        System.arraycopy(pRotateImage, 0, this.m_BmpData, offset3, nWidth * nHeight);
    }

    public byte[] toBytes() {
        return this.m_BmpData;
    }

    public class BITMAPFILEHEADER {
        public int bfOffBits;
        public short bfReserved1 = 0;
        public short bfReserved2 = 0;
        public int bfSize;
        public short bfType = 19778;

        public BITMAPFILEHEADER() {
        }

        public int sizeof() {
            return 14;
        }

        public byte[] toBytes() {
            byte[] m_Data = new byte[14];
            System.arraycopy(convet2bytes.short2bytes(this.bfType), 0, m_Data, 0, 2);
            System.arraycopy(convet2bytes.int2bytes(this.bfSize), 0, m_Data, 2, 4);
            System.arraycopy(convet2bytes.short2bytes(this.bfReserved1), 0, m_Data, 6, 2);
            System.arraycopy(convet2bytes.short2bytes(this.bfReserved2), 0, m_Data, 8, 2);
            System.arraycopy(convet2bytes.int2bytes(this.bfOffBits), 0, m_Data, 10, 4);
            return m_Data;
        }
    }

    public class RGBQUAD {
        public byte rgbBlue;
        public byte rgbGreen;
        public byte rgbRed;
        public byte rgbReserved = 0;

        public RGBQUAD() {
        }

        public byte[] GetGRBTableByteData() {
            byte[] m_Data = new byte[1024];
            int nOffset = 0;
            for (int i = 0; i < 256; i++) {
                m_Data[nOffset] = (byte) i;
                m_Data[nOffset + 1] = (byte) i;
                m_Data[nOffset + 2] = (byte) i;
                m_Data[nOffset + 3] = (byte) i;
                nOffset += 4;
            }
            return m_Data;
        }
    }

    public class BITMAPINFOHEADER {
        public short biBitCount = 8;
        public int biClrImportant = 0;
        public int biClrUsed = 0;
        public int biCompression = 0;
        public int biHeight;
        public short biPlanes = 1;
        public int biSize = 40;
        public int biSizeImage = 0;
        public int biWidth;
        public int biXPelsPerMeter = 19686;
        public int biYPelsPerMeter = 19686;

        public BITMAPINFOHEADER() {
        }

        public byte[] toBytes() {
            byte[] m_Data = new byte[40];
            System.arraycopy(convet2bytes.int2bytes(this.biSize), 0, m_Data, 0, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biWidth), 0, m_Data, 4, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biHeight), 0, m_Data, 8, 4);
            System.arraycopy(convet2bytes.short2bytes(this.biPlanes), 0, m_Data, 12, 2);
            System.arraycopy(convet2bytes.short2bytes(this.biBitCount), 0, m_Data, 14, 2);
            System.arraycopy(convet2bytes.int2bytes(this.biCompression), 0, m_Data, 16, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biSizeImage), 0, m_Data, 20, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biXPelsPerMeter), 0, m_Data, 24, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biYPelsPerMeter), 0, m_Data, 28, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biClrUsed), 0, m_Data, 32, 4);
            System.arraycopy(convet2bytes.int2bytes(this.biClrImportant), 0, m_Data, 36, 4);
            return m_Data;
        }
    }

    public class BITMAPINFO {
        public RGBQUAD bmiColors;
        public BITMAPINFOHEADER bmiHeader;

        public BITMAPINFO() {
            this.bmiHeader = new BITMAPINFOHEADER();
            this.bmiColors = new RGBQUAD();
        }

        public int sizeof() {
            return this.bmiHeader.biSize + 1024;
        }
    }

    public static class convet2bytes {
        public static byte[] short2bytes(short s) {
            return new byte[]{(byte) (s & 255), (byte) ((65280 & s) >> 8)};
        }

        public static byte[] int2bytes(int i) {
            return new byte[]{(byte) (i & 255), (byte) ((65280 & i) >> 8), (byte) ((16711680 & i) >> 16), (byte) ((-16777216 & i) >> 24)};
        }
    }
}
