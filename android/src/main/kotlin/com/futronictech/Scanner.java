package com.futronictech;

import android.content.Context;

public class Scanner {
    public static final byte FTRSCAN_INTERFACE_STATUS_CONNECTED = 0;
    public static final byte FTRSCAN_INTERFACE_STATUS_DISCONNECTED = 1;
    public static final int FTR_MAX_INTERFACE_NUMBER = 128;
    public final int FTR_ERROR_EMPTY_FRAME = 4306;
    public final int FTR_ERROR_FIRMWARE_INCOMPATIBLE = 536870917;
    public final int FTR_ERROR_HARDWARE_INCOMPATIBLE = 536870916;
    public final int FTR_ERROR_INVALID_AUTHORIZATION_CODE = 536870918;
    public final int FTR_ERROR_MOVABLE_FINGER = 536870913;
    public final int FTR_ERROR_NOT_ENOUGH_MEMORY = 8;
    public final int FTR_ERROR_NOT_READY = 21;
    public final int FTR_ERROR_NO_ERROR = 0;
    public final int FTR_ERROR_NO_FRAME = 536870914;
    public final int FTR_ERROR_WRITE_PROTECT = 19;
    public final int FTR_LOG_LEVEL_FULL = 2;
    public final int FTR_LOG_LEVEL_MIN = 0;
    public final int FTR_LOG_LEVEL_OPTIMAL = 1;
    public final int FTR_LOG_MASK_OFF = 0;
    public final int FTR_LOG_MASK_PROCESS_ID = 16;
    public final int FTR_LOG_MASK_THREAD_ID = 8;
    public final int FTR_LOG_MASK_TIMESTAMP = 4;
    public final int FTR_LOG_MASK_TO_AUX = 2;
    public final int FTR_LOG_MASK_TO_FILE = 1;
    public final int FTR_OPTIONS_CHECK_FAKE_REPLICA = 1;
    public final int FTR_OPTIONS_DETECT_FAKE_FINGER = 1;
    public final int FTR_OPTIONS_IMPROVE_IMAGE = 32;
    public final int FTR_OPTIONS_INVERT_IMAGE = 64;
    private final int kDefaultDeviceInstance = 0;
    private int m_ErrorCode;
    private int m_ImageHeight = 0;
    private int m_ImageWidth = 0;
    private long m_hDevice = 0;

    private native boolean OpenDeviceCtx(Object obj);

    public native boolean CloseDevice();

    public native boolean GetDiodesStatus(byte[] bArr);

    public native boolean GetFrame(byte[] bArr);

    public native boolean GetImage(int i, byte[] bArr);

    public native boolean GetImage2(int i, byte[] bArr);

    public native boolean GetImageByVariableDose(int i, byte[] bArr);

    public native boolean GetImageSize();

    public native boolean GetInterfaces(byte[] bArr);

    public native String GetVersionInfo();

    public native boolean IsFingerPresent();

    public native boolean OpenDevice();

    public native boolean OpenDeviceOnInterface(int i);

    public native boolean Restore7Bytes(byte[] bArr);

    public native boolean RestoreSecret7Bytes(byte[] bArr, byte[] bArr2);

    public native boolean Save7Bytes(byte[] bArr);

    public native boolean SaveSecret7Bytes(byte[] bArr, byte[] bArr2);

    public native boolean SetDiodesStatus(int i, int i2);

    public native boolean SetGlobalSyncDir(String str);

    public native boolean SetLogOptions(int i, int i2);

    public native boolean SetNewAuthorizationCode(byte[] bArr);

    public native boolean SetOptions(int i, int i2);

    public int GetImageWidth() {
        return this.m_ImageWidth;
    }

    public int GetImaegHeight() {
        return this.m_ImageHeight;
    }

    public int GetErrorCode() {
        return this.m_ErrorCode;
    }

    public String GetErrorMessage() {
        switch (this.m_ErrorCode) {
            case 0:
                return "OK";
            case 19 /*19*/:
                return "Write Protect";
            case 4306:
                return "Empty Frame";
            case 536870913:
                return "Moveable Finger";
            case 536870914:
                return "Fake Finger";
            case 536870916:
                return "Hardware Incompatible";
            case 536870917:
                return "Firmware Incompatible";
            case 536870918:
                return "Invalid Authorization Code";
            default:
                return String.format("Error code is %d", new Object[]{Integer.valueOf(this.m_ErrorCode)});
        }
    }

    public boolean OpenDeviceOnInterfaceUsbHost(UsbDeviceDataExchangeImpl usb_host_ctx) {
        boolean res;
        synchronized (this) {
            res = OpenDeviceCtx(usb_host_ctx);
        }
        return res;
    }

    public boolean GetInterfacesUsbHost(Context ctx, byte[] pInterfaceList) {
        if (pInterfaceList.length < 128) {
            this.m_ErrorCode = 8;
            return false;
        }
        UsbDeviceDataExchangeImpl.GetInterfaces(ctx, pInterfaceList);
        return true;
    }

    public void CloseDeviceUsbHost() {
        synchronized (this) {
            CloseDevice();
        }
    }

    public long GetDeviceHandle() {
        return this.m_hDevice;
    }

    static {
        System.loadLibrary("usb-1.0");
        System.loadLibrary("ftrScanAPI");
        System.loadLibrary("ftrScanApiAndroidJni");
    }
}
