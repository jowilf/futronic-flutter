#import "FutronicFlutterPlugin.h"
#if __has_include(<futronic_flutter/futronic_flutter-Swift.h>)
#import <futronic_flutter/futronic_flutter-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "futronic_flutter-Swift.h"
#endif

@implementation FutronicFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFutronicFlutterPlugin registerWithRegistrar:registrar];
}
@end
