import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:file/memory.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:futronic_flutter/futronic_flutter.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  ScanViewController controller;
  String capture;
  Uint8List cap;
  var i = 0;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FutronicScanner.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: <Widget>[
            Center(
                child: Container(
                    padding: EdgeInsets.symmetric(vertical: 30.0),
                    width: 300.0,
                    height: 300.0,
                    child: FutronicScanner(
                      onTextViewCreated: _onScanViewCreated,
                    ))),
            SizedBox(height: 20),
            if (cap != null)
              Image.file(
                MemoryFileSystem().file('cap$i')..writeAsBytesSync(cap),
                width: 100,
              ),
            if (capture != null)
              Image.file(
                File(capture),
                width: 100,
              ),
            Row(
              children: <Widget>[
                FlatButton(
                    onPressed: () async {
                      var res = await controller.look();
                      i++;
                      if (res is Uint8List)
                        setState(() {
                          print("get bytes");
                          print(res);
                          cap = res;
                        });
                    },
                    child: Text('Scan')),
                FlatButton(
                    onPressed: () async {
                      controller.scan();
                     /* var res = await controller.capture();
                      setState(() {
                        capture = res;
                      });*/
                    },
                    child: Text('Capture')),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _onScanViewCreated(ScanViewController controller) {
    //controller.setText('Hello from Android Native!');
    this.controller = controller;
    controller.scan();
    //controller.scan();
  }
}
