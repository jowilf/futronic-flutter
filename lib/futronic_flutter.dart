import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

typedef void TextViewCreatedCallback(ScanViewController controller);

class FutronicScanner extends StatefulWidget {
  final TextViewCreatedCallback onTextViewCreated;

  FutronicScanner({this.onTextViewCreated});

  static const MethodChannel _channel = const MethodChannel('futronic_flutter');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  @override
  State<StatefulWidget> createState() => FutronicScannerState();
}

class FutronicScannerState extends State<FutronicScanner> {
  @override
  Widget build(BuildContext context) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return AndroidView(
          viewType: 'futronic_scanner',
          onPlatformViewCreated: _onPlatformViewCreated);
    }
  }

  void _onPlatformViewCreated(int id) {
    if (widget.onTextViewCreated == null) {
      return;
    }
    widget.onTextViewCreated(new ScanViewController._(id));
  }
}

class ScanViewController {
  ScanViewController._(int id)
      : _channel = new MethodChannel('scanner_view_$id');

  final MethodChannel _channel;

  Future<void> setText(String text) async {
    assert(text != null);
    return _channel.invokeMethod('setText', text);
  }

  Future<void> scan() async {
    if (await Permission.storage.request().isGranted) {
      return _channel.invokeMethod('scan');
    }
  }

  Future<Uint8List> look() async {
    if (await Permission.storage.request().isGranted) {
      return _channel.invokeMethod('look');
    }
  }

  Future<String> capture() async {
    if (await Permission.storage.request().isGranted) {
      var res = await _channel.invokeMethod('capture');
      print("capture : $res");
      return res;
    }
  }
}
